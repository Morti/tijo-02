package com.company;
import java.util.ArrayList;
import java.util.List;

/**
 * A class implementing the LineConverter interface
 */
public class LineConverterImpl implements LineConverter {
    // klasa LineConverterImpl powinna implementowac interfejs LineConverter
    // prosze zaimplementowac metode toTeam
    // przed implementacja prosze przeanalizowac klase Main
    /**
     * LineConverterImpl class should implement the Line Converter interface
     * @param lines
     * @param splitBy this is the sign of the separator
     * @return a list of Team objects
     */
    @Override
    public List<Team> toTeam(List<String> lines, String splitBy) {
        List<Team> teams = new ArrayList<>();
        for(String line: lines) {
            String[] foo = line.split(splitBy);
            Team team = new Team(Integer.parseInt(foo[0].trim()),
                    foo[1],
                    Integer.parseInt(foo[2].trim()),
                    Integer.parseInt(foo[3].trim()),
                    Integer.parseInt(foo[4].trim()),
                    Integer.parseInt(foo[5].trim()),
                    Integer.parseInt(foo[6].trim()),
                    foo[7]);
            teams.add(team);
        }
        return teams;
    }
}