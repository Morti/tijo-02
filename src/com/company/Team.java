package com.company;

/**
 *  Represents a team in league table
 */
public class Team {
    // prosze zaprojektowac klase Team
    // prosze zadbac o odpowiednia implementacje pol klasy
    // prosze zaimplementowac odpowiedni konstruktor oraz metody,
    // ktore zwracaja pola klasy
    // prosze zaimplementowac metode toString
    // przed implementacja prosze przeanalizowac plik CSV
    // (http://tomaszgadek.com/static/ekstraklasa.csv)
    // oraz klase Main

    private int id;
    private String name;
    private int games;
    private int points;
    private int wins;
    private int draws;
    private int loses;
    private String goals;

    /**
     *
     * @param id Place in league table
     * @param name Team name
     * @param games Number of games
     * @param points The number of points scored by the team
     * @param wins Number of wins
     * @param draws Number of draws
     * @param loses Number of loses
     * @param goals Ratio of the number of scored and lost goals
     */
    public Team(int id, String name, int games, int points, int wins, int draws, int loses, String goals) {
        this.id = id;
        this.name = name;
        this.games = games;
        this.points = points;
        this.wins = wins;
        this.draws = draws;
        this.loses = loses;
        this.goals = goals;
    }

    /**
     * Get the place of this Team
     * @return this Team place in league table
     */
    public int getId() {
        return id;
    }

    /**
     * Change the place of this Team
     * @param id Place in league table
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the name of this Team
     * @return this Team name
     */
    public String getName() {
        return name;
    }

    /**
     * Change the name of this Team
     * @param name name of this Team
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the games of this Team
     * @return this Team games
     */
    public int getGames() {
        return games;
    }

    /**
     * Change games of this Team
     * @param games number of games of this team
     */
    public void setGames(int games) {
        this.games = games;
    }

    /**
     * Get points of this Team
     * @return this Team points
     */
    public int getPoints() {
        return points;
    }

    /**
     * Change points of this Team
     * @param points number of points of this Team
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Get wins of this Team
     * @return this Team wins
     */
    public int getWins() {
        return wins;
    }

    /**
     * Change wins of this Team
     * @param wins number of wins of this Team
     */
    public void setWins(int wins) {
        this.wins = wins;
    }

    /**
     * Get draws of this Team
     * @return this Team draws
     */
    public int getDraws() {
        return draws;
    }

    /**
     * Change draws of this Team
     * @param draws number of draws of this Team
     */
    public void setDraws(int draws) {
        this.draws = draws;
    }

    /**
     * Get loses of this Team
     * @return this Team loses
     */
    public int getLoses() {
        return loses;
    }

    /**
     * Change loses of this Team
     * @param loses number of loses of this Team
     */
    public void setLoses(int loses) {
        this.loses = loses;
    }

    /**
     * Get goals ratio of this Team
     * @return this Team goals ratio
     */
    public String getGoals() {
        return goals;
    }

    /**
     * Change goals ratio of this Team
     * @param goals goals ratio of this Team
     */
    public void setGoals(String goals) {
        this.goals = goals;
    }

    /**
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", games=" + games +
                ", points=" + points +
                ", wins=" + wins +
                ", draws=" + draws +
                ", loses=" + loses +
                ", goals='" + goals + '\'' +
                '}';
    }
}