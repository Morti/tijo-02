package com.company;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CsvReader is the class used to read data from a csv file
 */
public class CsvReader implements CsvReport {
    // klasa CsvReader powinna implementowac interfejs CsvReport
    // prosze utworzyc odpowiedni konstruktor
    // oraz wlasna implementacje metody csvToListOfString
    // przed implementacja prosze przeanalizowac klase Main
    private String csv_file;

    /**
     * Constructor for class CsvReader
     * @param csv_file is a path to the csv file
     */
    public CsvReader(String csv_file) {
        this.csv_file = csv_file;
    }

    /**
     * Making list from csv files
     * @return this list of league table
     *         Read from ekstraklasa.csv file
     */
    @Override
    public List<String> csvToListOfString() {

        BufferedReader br = null;
        String line = "";
        List<String> leagueTable = new ArrayList<>();

        try {
            br = new BufferedReader(new FileReader(csv_file));
            br.readLine();
            while ((line = br.readLine()) != null) {
                leagueTable.add(line);
            }
            return leagueTable;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return leagueTable;
    }
}